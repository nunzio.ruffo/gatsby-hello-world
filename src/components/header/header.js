import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import './header.scss';

const Header = ({ siteTitle }) => {
  return (
    <div className="container">
      <nav className="navbar px-4 py-2 mt-4 is-light" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item ml-2">
            <h2>Solo Gatsby!</h2>
          </Link>
        </div>
        <div className="navbar-menu">
          <div className="navbar-start">
            <Link to="/items/" className="navbar-item">
              Items
            </Link>
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              <Link to="/login/" className="navbar-item">
                <strong>Login</strong>
              </Link>
            </div>
          </div>
           
        </div>
      </nav>
    </div>
  )
}


Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
